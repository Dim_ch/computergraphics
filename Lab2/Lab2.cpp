﻿// Lab2.cpp : Определяет точку входа для приложения.
//

#include "framework.h"
#include "Lab2.h"
#include <fstream>
#include <string>
#include <iostream>

using namespace Gdiplus;
#pragma comment (lib,"Gdiplus.lib")

#define MAX_LOADSTRING 100

// Глобальные переменные:
HINSTANCE hInst;                                // текущий экземпляр
WCHAR szTitle[MAX_LOADSTRING];                  // Текст строки заголовка
WCHAR szWindowClass[MAX_LOADSTRING];            // имя класса главного окна

// Отправить объявления функций, включенных в этот модуль кода:
ATOM                MyRegisterClass(HINSTANCE hInstance);
BOOL                InitInstance(HINSTANCE, int);
LRESULT CALLBACK    WndProc(HWND, UINT, WPARAM, LPARAM);
INT_PTR CALLBACK    About(HWND, UINT, WPARAM, LPARAM);

struct VIEPORT {
    REAL left;
    REAL top;
    REAL right;
    REAL bottom;
};
const int functionPointsCount = 500;
const float R = 10.0f;
const VIEPORT world1 = { -1.0f, 1.0f, 1.0f, -1.0f },
              world2 = { -R, R, R, -R };
PointF* function_Points = NULL;
PointF* polygonPointsWorld = NULL;
int polygonPointsWorldCount = 0;
PointF pointsToCheck[1];

VOID OnPaint(HDC hdc, HWND hWnd);
void worldToViewport(PointF* points, int count, VIEPORT vieport, VIEPORT World);
int pointBelongPolygon(PointF* points, int pointsCount, PointF pointToCheck);
FLOAT CalcDotProduct(PointF n1, PointF n2);
float Function(float t);
PointF* Plot(float xMin, float xMax, size_t n);
void DrawFunction(PointF* points, size_t count, PointF LT, PointF RB, Gdiplus::Graphics* graphics);
void LiangBarskyClipper(PointF A, PointF B, REAL left, REAL top, REAL right, REAL bottom, Gdiplus::Graphics* graphics, Pen* pen);
void GetPointsPolygon();

int APIENTRY wWinMain(_In_ HINSTANCE hInstance,
                     _In_opt_ HINSTANCE hPrevInstance,
                     _In_ LPWSTR    lpCmdLine,
                     _In_ int       nCmdShow)
{
    UNREFERENCED_PARAMETER(hPrevInstance);
    UNREFERENCED_PARAMETER(lpCmdLine);

    // Инициализация GDI+.
    GdiplusStartupInput gdiplusStartupInput;
    ULONG_PTR           gdiplusToken;
    Status              stRet = GdiplusStartup(&gdiplusToken, &gdiplusStartupInput, NULL);

    if (Ok != stRet) return FALSE;

    // Инициализация глобальных строк
    LoadStringW(hInstance, IDS_APP_TITLE, szTitle, MAX_LOADSTRING);
    LoadStringW(hInstance, IDC_LAB2, szWindowClass, MAX_LOADSTRING);
    MyRegisterClass(hInstance);

    // Выполнить инициализацию приложения:
    if (!InitInstance (hInstance, nCmdShow))
    {
        return FALSE;
    }

    HACCEL hAccelTable = LoadAccelerators(hInstance, MAKEINTRESOURCE(IDC_LAB2));

    MSG msg;

    // Цикл основного сообщения:
    while (GetMessage(&msg, nullptr, 0, 0))
    {
        if (!TranslateAccelerator(msg.hwnd, hAccelTable, &msg))
        {
            TranslateMessage(&msg);
            DispatchMessage(&msg);
        }
    }

    GdiplusShutdown(gdiplusToken);
    return (int) msg.wParam;
}



//
//  ФУНКЦИЯ: MyRegisterClass()
//
//  ЦЕЛЬ: Регистрирует класс окна.
//
ATOM MyRegisterClass(HINSTANCE hInstance)
{
    WNDCLASSEXW wcex;

    wcex.cbSize = sizeof(WNDCLASSEX);

    wcex.style          = CS_HREDRAW | CS_VREDRAW;
    wcex.lpfnWndProc    = WndProc;
    wcex.cbClsExtra     = 0;
    wcex.cbWndExtra     = 0;
    wcex.hInstance      = hInstance;
    wcex.hIcon          = LoadIcon(hInstance, MAKEINTRESOURCE(IDI_LAB2));
    wcex.hCursor        = LoadCursor(nullptr, IDC_ARROW);
    wcex.hbrBackground  = (HBRUSH)(COLOR_WINDOW+1);
    wcex.lpszMenuName   = 0;
    wcex.lpszClassName  = szWindowClass;
    wcex.hIconSm        = LoadIcon(wcex.hInstance, MAKEINTRESOURCE(IDI_SMALL));

    return RegisterClassExW(&wcex);
}

//
//   ФУНКЦИЯ: InitInstance(HINSTANCE, int)
//
//   ЦЕЛЬ: Сохраняет маркер экземпляра и создает главное окно
//
//   КОММЕНТАРИИ:
//
//        В этой функции маркер экземпляра сохраняется в глобальной переменной, а также
//        создается и выводится главное окно программы.
//
BOOL InitInstance(HINSTANCE hInstance, int nCmdShow)
{
   hInst = hInstance; // Сохранить маркер экземпляра в глобальной переменной

   HWND hWnd = CreateWindowW(szWindowClass, szTitle, WS_OVERLAPPEDWINDOW,
      CW_USEDEFAULT, 0, CW_USEDEFAULT, 0, nullptr, nullptr, hInstance, nullptr);

   if (!hWnd)
   {
      return FALSE;
   }

   ShowWindow(hWnd, nCmdShow);
   UpdateWindow(hWnd);

   return TRUE;
}

//
//  ФУНКЦИЯ: WndProc(HWND, UINT, WPARAM, LPARAM)
//
//  ЦЕЛЬ: Обрабатывает сообщения в главном окне.
//
//  WM_COMMAND  - обработать меню приложения
//  WM_PAINT    - Отрисовка главного окна
//  WM_DESTROY  - отправить сообщение о выходе и вернуться
//
//
LRESULT CALLBACK WndProc(HWND hWnd, UINT message, WPARAM wParam, LPARAM lParam)
{
    switch (message)
    {
    case WM_CREATE:
        function_Points = Plot(0.f, M_PI * 4, functionPointsCount);
        GetPointsPolygon();
        break;
    case WM_COMMAND:
        break;
    case WM_PAINT:
        {
            PAINTSTRUCT ps;
            HDC hdc = BeginPaint(hWnd, &ps);
            OnPaint(hdc, hWnd);
            EndPaint(hWnd, &ps);
        }
        break;
    case WM_DESTROY:
        PostQuitMessage(0);
        delete[] function_Points;
        delete[] polygonPointsWorld;
        break;
    default:
        return DefWindowProc(hWnd, message, wParam, lParam);
    }
    return 0;
}

VOID OnPaint(HDC hdc, HWND hWnd) {
    Gdiplus::Graphics  graphics(hdc);
    graphics.Clear(Color::White);
    graphics.SetSmoothingMode(SmoothingMode::SmoothingModeAntiAlias);
    RECT winRect = { 0 };
    GetClientRect(hWnd, &winRect);

    SolidBrush brushPointCheckFalse(Color::Red);
    SolidBrush brushPointCheckTrue(Color::Green);
    SolidBrush* brushPoint = &brushPointCheckFalse;
    Pen       penBlack(Color::Black);

    PointF* polygonPointsWorld1 = new PointF[polygonPointsWorldCount];
    for (int i = 0; i < polygonPointsWorldCount; ++i) {
        polygonPointsWorld1[i] = polygonPointsWorld[i];
    }

    TCHAR msgRes[64] = TEXT("");
    switch (pointBelongPolygon(polygonPointsWorld1, polygonPointsWorldCount, pointsToCheck[0])) {
        case 0:
            StringCchCopy(msgRes, 64, TEXT("Точка не принадлежит многоугольнику"));
            brushPoint = &brushPointCheckFalse;
            break;
        case 1:
            StringCchCopy(msgRes, 64, TEXT("Точка принадлежит многоугольнику"));
            brushPoint = &brushPointCheckTrue;
            break;
        case 2:
            StringCchCopy(msgRes, 64, TEXT("Точка лежит на границе многоугольника"));
            brushPoint = &brushPointCheckTrue;
            break;
        case 3:
            StringCchCopy(msgRes, 64, TEXT("Точка совпадает с вершиной многоугольника"));
            brushPoint = &brushPointCheckTrue;
            break;
        default:
            StringCchCopy(msgRes, 64, TEXT("----------------"));
    }

    PointF functionPoints[functionPointsCount];
    for (int i = 0; i < functionPointsCount; ++i) {
        functionPoints[i] = function_Points[i];
    }

    // Координаты ограничивающего прямоугольника (left, top), (right, bottom)
    PointF limitBoxPoint[2] = { PointF(-5.f, R), PointF(5.f, -R) };

    REAL width;
    VIEPORT viewport1, viewport2;
    if (winRect.right > winRect.bottom) {
        width = winRect.right / 2;
        viewport2.left = width + 5;
        if (width > winRect.bottom) width = winRect.bottom;
        viewport1.top = viewport1.left = 5;
        viewport1.right = viewport1.bottom = width - 10;

        viewport2.top = 5;
        viewport2.right = width - 10;
        viewport2.bottom = width - 10;
    }
    else {
        width = winRect.bottom / 2;
        viewport2.top = width + 5;
        if (width > winRect.right) width = winRect.right;
        viewport1.top = viewport1.left = 5;
        viewport1.right = viewport1.bottom = width - 10;
        viewport2.left = 5;
        viewport2.right = viewport2.bottom = width - 10;
    }

    worldToViewport(polygonPointsWorld1, polygonPointsWorldCount, viewport1, world1);
    worldToViewport(pointsToCheck, 1, viewport1, world1);
    worldToViewport(functionPoints, functionPointsCount, viewport2, world2);
    worldToViewport(limitBoxPoint, 2, viewport2, world2);

    DrawFunction(functionPoints, functionPointsCount, PointF(limitBoxPoint[0].X, limitBoxPoint[1].Y), 
                 PointF(limitBoxPoint[1].X, limitBoxPoint[0].Y), &graphics);
    graphics.DrawPolygon(&penBlack, polygonPointsWorld1, polygonPointsWorldCount);

    RectF rect(pointsToCheck[0].X - 2.5f, pointsToCheck[0].Y - 2.5f, 5.0f, 5.0f);
    graphics.FillEllipse(brushPoint, rect);

    // Вывод текста
    SolidBrush fontBrush(Color::Black);
    Font font(L"Bookman Old Style", 10.0f, FontStyleBoldItalic);
    StringFormat strFormat;
    strFormat.SetAlignment(StringAlignment::StringAlignmentFar); // по горизонтали
    strFormat.SetLineAlignment(StringAlignment::StringAlignmentNear); // по вертикали
    graphics.DrawString(msgRes, -1, &font,
        PointF(viewport1.right + viewport1.left, viewport1.top),
        &strFormat, &fontBrush);

    delete[] polygonPointsWorld1;
}

//
// Функция pointBelongPolygon
//
// Цель: проверяет принадлежность точки многоугольнику
//
// Результат:
//      0 - не принадлежит многоугольнику
//      1 - принадлежит многоугольнику
//      2 - лежит на границе
//      3 - совпадает с вершиной
int pointBelongPolygon(PointF *points, int pointsCount, PointF pointToCheck) {
    PointF Normal, PA;
    FLOAT dotProdPrev = 0, dotProd;
    int zeroCount = 0;
    for (int i = 0; i < pointsCount; ++i) {
        if (pointToCheck.Equals(points[i]))
            return 3;

        if (i == pointsCount - 1) {
            Normal.X = points[0].Y - points[i].Y;
            Normal.Y = points[i].X - points[0].X;
        }
        else {
            Normal.X = points[i + 1].Y - points[i].Y;
            Normal.Y = points[i].X - points[i + 1].X;
        }

        PA.X = pointToCheck.X - points[i].X;
        PA.Y = pointToCheck.Y - points[i].Y;

        dotProd = CalcDotProduct(Normal, PA);

        if (dotProd == 0) {
            zeroCount++;
        }

        if (i > 0 && ((dotProd > 0 && dotProdPrev < 0) || (dotProd < 0 && dotProdPrev > 0)))
            return 0;

        dotProdPrev = dotProd;
    }

    if (zeroCount == 1) {
        return 2;
    }
    if (zeroCount == 2) {
        return 3;
    }

    return 1;
}

FLOAT CalcDotProduct(PointF n1, PointF n2) {
    return n1.X * n2.X + n1.Y * n2.Y;
}

//
// Функция worldToViewport
//
// Цель: преобразует координаты мирового окна в координаты порта просмотра
void worldToViewport(PointF* points, int count, VIEPORT vieport, VIEPORT World) {
    REAL A, B, C, D;
    A = vieport.right / (World.right - World.left);
    B = vieport.bottom / (World.bottom - World.top);
    C = vieport.left - A * World.left;
    D = vieport.top - B * World.top;

    for (int i = 0; i < count; ++i) {
        points[i].X = A * points[i].X + C;
        points[i].Y = B * points[i].Y + D;
    }
}

inline float Sin(float angle) {
    __asm {
        fld angle
        fsin
    }
}

inline float Cos(float angle) {
    __asm {
        fld angle
        fcos
    }
}

PointF* Plot(float xMin, float xMax, size_t n) {
    PointF* points = new PointF[n];
    if (n > 1) {
        float dx = (xMax - xMin) / (float)(n - 1);

        for (size_t i = 0; i < n; ++i) {
            float x = xMin + dx * i;
            float q = Function(x);
            points[i] = PointF(q * Cos(x), q * Sin(x));
        }
    }
    return points;
}

float Function(float t) {
    return R * Sin(5.0f * t / 2.0f);
}

void DrawFunction(PointF* points, size_t count, PointF LT, PointF RB, Gdiplus::Graphics* graphics) {
    Pen linePen(Color::Black);
    RectF rectangle(LT.X, RB.Y, RB.X - LT.X, LT.Y - RB.Y);
    graphics->DrawRectangle(&linePen, rectangle);
    for (size_t i = 0; i < count - 1; ++i) {
        LiangBarskyClipper(points[i], points[i+1], LT.X, LT.Y, RB.X, RB.Y, graphics, &linePen);
    }
}

void LiangBarskyClipper(PointF A, PointF B, REAL left, REAL top, REAL right, REAL bottom, Gdiplus::Graphics* graphics, Pen* pen) {
    REAL P[4];
    P[0] = -(B.X - A.X);
    P[1] = -P[0]; // ax
    P[2] = -(B.Y - A.Y);
    P[3] = -P[2]; // ay

    if ((P[0] == 0.0f) || (P[2] == 0.0f)) return;

    REAL Q[4];
    Q[0] = A.X - left;
    Q[1] = right - A.X;
    Q[2] = A.Y - bottom;
    Q[3] = top - A.Y;

    REAL Tout = 1, Tin = 0, Thit;
    for (int i = 0; i < 4; ++i) {
        Thit = Q[i] / P[i];
        if (P[i] < 0.0f) { // прямая входит в прямоугольник
            if (Thit > Tin) Tin = Thit;
        }
        else { // прямая выходит из прямоугольника
            if (Thit < Tout) Tout = Thit;
        }
    }

    if (Tin > Tout) return;

    PointF A1, B1;
    A1.X = A.X + P[1] * Tin;
    A1.Y = A.Y + P[3] * Tin;
    B1.X = A.X + P[1] * Tout;
    B1.Y = A.Y + P[3] * Tout;

    graphics->DrawLine(pen, A1, B1);
}

void GetPointsPolygon() {
    TCHAR path[300];
    TCHAR pathFile[400];
    TCHAR format[] = TEXT("%s\\%s");
    TCHAR file[] = TEXT("points.txt");
    if (GetCurrentDirectory(300, path) != 0) {
        if (S_OK == StringCchPrintf(pathFile, 400, format, path, file)) {
            std::ifstream in(pathFile);          // поток для записи
            if (in.is_open())
            {
                std::string num;
                in >> num;
                long count = stol(num);
                polygonPointsWorld = new PointF[count];
                polygonPointsWorldCount = count;
                std::string x, y;
                for (long i = 0; i < count; ++i) {
                    in >> x;
                    in >> y;
                    polygonPointsWorld[i] = PointF(stof(x), stof(y));
                }
                in >> x;
                in >> y;
                pointsToCheck[0] = PointF(stof(x), stof(y));
                in.close();
            }

        }
    }
}