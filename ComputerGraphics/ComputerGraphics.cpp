﻿// ComputerGraphics.cpp : Определяет точку входа для приложения.
//

#include "framework.h"
#include "ComputerGraphics.h"

using namespace Gdiplus;
#pragma comment (lib,"Gdiplus.lib")


// Глобальные переменные:
#define MAX_LOADSTRING 100
PointF* GetShipBreakDown(PointF origin, int r);

HINSTANCE hInst;                                // текущий экземпляр
WCHAR     szTitle[MAX_LOADSTRING];                  // Текст строки заголовка
WCHAR     szWindowClass[MAX_LOADSTRING];            // имя класса главного окна
Image*    woodTexture1;
Image*    woodTexture2;
Image*    waterTexture;
Image*    skyTexture;
Image*    shipImg;

// Отправить объявления функций, включенных в этот модуль кода:
ATOM                MyRegisterClass(HINSTANCE hInstance);
BOOL                InitInstance(HINSTANCE, int);
LRESULT CALLBACK    WndProc(HWND, UINT, WPARAM, LPARAM);
INT_PTR CALLBACK    About(HWND, UINT, WPARAM, LPARAM);

// Функции отрисовки
VOID OnPaint(HDC hdc, HWND hWnd);

int APIENTRY wWinMain(_In_ HINSTANCE hInstance,
                     _In_opt_ HINSTANCE hPrevInstance,
                     _In_ LPWSTR    lpCmdLine,
                     _In_ int       nCmdShow)
{
    UNREFERENCED_PARAMETER(hPrevInstance);
    UNREFERENCED_PARAMETER(lpCmdLine);

    // Инициализация GDI+.
    GdiplusStartupInput gdiplusStartupInput;
    ULONG_PTR           gdiplusToken;
    Status              stRet = GdiplusStartup(&gdiplusToken, &gdiplusStartupInput, NULL);

    if (Ok != stRet) return FALSE;

    // Инициализация глобальных строк
    LoadStringW(hInstance, IDS_APP_TITLE, szTitle, MAX_LOADSTRING);
    LoadStringW(hInstance, IDC_COMPUTERGRAPHICS, szWindowClass, MAX_LOADSTRING);
    MyRegisterClass(hInstance);

    // Выполнить инициализацию приложения:
    if (!InitInstance (hInstance, nCmdShow))
    {
        return FALSE;
    }

    HACCEL hAccelTable = LoadAccelerators(hInstance, MAKEINTRESOURCE(IDC_COMPUTERGRAPHICS));

    MSG msg;

    // Цикл основного сообщения:
    while (GetMessage(&msg, nullptr, 0, 0))
    {
        if (!TranslateAccelerator(msg.hwnd, hAccelTable, &msg))
        {
            TranslateMessage(&msg);
            DispatchMessage(&msg);
        }
    }

    GdiplusShutdown(gdiplusToken);
    return (int) msg.wParam;
}



//
//  ФУНКЦИЯ: MyRegisterClass()
//
//  ЦЕЛЬ: Регистрирует класс окна.
//
ATOM MyRegisterClass(HINSTANCE hInstance)
{
    WNDCLASSEXW wcex;

    wcex.cbSize = sizeof(WNDCLASSEX);

    wcex.style          = CS_HREDRAW | CS_VREDRAW;
    wcex.lpfnWndProc    = WndProc;
    wcex.cbClsExtra     = 0;
    wcex.cbWndExtra     = 0;
    wcex.hInstance      = hInstance;
    wcex.hIcon          = LoadIcon(hInstance, MAKEINTRESOURCE(IDI_COMPUTERGRAPHICS));
    wcex.hCursor        = LoadCursor(nullptr, IDC_ARROW);
    wcex.hbrBackground  = (HBRUSH)(COLOR_WINDOW+1);
    wcex.lpszMenuName   = MAKEINTRESOURCEW(0);
    wcex.lpszClassName  = szWindowClass;
    wcex.hIconSm        = LoadIcon(wcex.hInstance, MAKEINTRESOURCE(IDI_SMALL));

    return RegisterClassExW(&wcex);
}

//
//   ФУНКЦИЯ: InitInstance(HINSTANCE, int)
//
//   ЦЕЛЬ: Сохраняет маркер экземпляра и создает главное окно
//
//   КОММЕНТАРИИ:
//
//        В этой функции маркер экземпляра сохраняется в глобальной переменной, а также
//        создается и выводится главное окно программы.
//
BOOL InitInstance(HINSTANCE hInstance, int nCmdShow)
{
   hInst = hInstance; // Сохранить маркер экземпляра в глобальной переменной

   woodTexture1 = Image(L"wood_texture1.jpg").GetThumbnailImage(50, 50);
   woodTexture2 = Image(L"wood_texture2.jpg").GetThumbnailImage(50, 50);
   waterTexture = Image(L"water_texture1.jpg").GetThumbnailImage(400, 400);
   skyTexture   = Image(L"sky_texture.jpg").GetThumbnailImage(400, 400);
   shipImg      = new Image(L"ship.jpg");

   HWND hWnd = CreateWindowW(szWindowClass, szTitle, WS_OVERLAPPEDWINDOW,
      CW_USEDEFAULT, 0, CW_USEDEFAULT, 0, nullptr, nullptr, hInstance, nullptr);

   if (!hWnd)
   {
      return FALSE;
   }

   ShowWindow(hWnd, nCmdShow);
   UpdateWindow(hWnd);

   return TRUE;
}

//
//  ФУНКЦИЯ: WndProc(HWND, UINT, WPARAM, LPARAM)
//
//  ЦЕЛЬ: Обрабатывает сообщения в главном окне.
//
//  WM_COMMAND  - обработать меню приложения
//  WM_PAINT    - Отрисовка главного окна
//  WM_DESTROY  - отправить сообщение о выходе и вернуться
//
//
LRESULT CALLBACK WndProc(HWND hWnd, UINT message, WPARAM wParam, LPARAM lParam)
{
    switch (message)
    {
    case WM_COMMAND:
        break;
    case WM_PAINT:
        {
            PAINTSTRUCT ps;
            HDC hdc = BeginPaint(hWnd, &ps);
            OnPaint(hdc, hWnd);
            EndPaint(hWnd, &ps);
        }
        break;
    case WM_DESTROY:
        if (woodTexture1 != NULL) delete woodTexture1;
        if (woodTexture2 != NULL) delete woodTexture2;
        if (waterTexture != NULL) delete waterTexture;
        if (skyTexture != NULL) delete skyTexture;
        PostQuitMessage(0);
        break;
    default:
        return DefWindowProc(hWnd, message, wParam, lParam);
    }
    return 0;
}

void DrawShip(Graphics *graphics, RECT winRect) {

    int x = winRect.right / 2 - 252;
    int y = winRect.bottom / 2;
    // *********** Рубка коробля
    Pen    shipCabinLinePen(Color(122, 177, 243), 10.0f);
    Pen    shipCabinPen(Color(35, 63, 95));
    Rect   shipCabinRect(x + 150, y - 50, 200, 90);

    const int DoorPointsCount = 6;
    Point DoorPoints[DoorPointsCount]{
        Point(x + 170, y + 40), Point(x + 170, y - 10), Point(x + 180, y - 25),
        Point(x + 210, y - 25), Point(x + 220, y - 10), Point(x + 220, y + 40)
    };

    PointF* pointsHall1 = GetShipBreakDown(PointF(x + 190, y), 10);
    PointF* pointsHall2 = GetShipBreakDown(PointF(x + 400, y + 40), 30);
    PointF* pointsHall3 = GetShipBreakDown(PointF(x + 290, y - 30), 10);
    PointF* pointsHall4 = GetShipBreakDown(PointF(x + 110, y + 60), 40);
    Rect rectHall1(x + 200, y - 100, 15, 15);
    Rect rectHall2(x + 250, y + 70, 60, 40);
    GraphicsPath path;
    path.AddPolygon(pointsHall1, 8);
    path.AddPolygon(pointsHall2, 8);
    path.AddPolygon(pointsHall3, 8);
    path.AddPolygon(pointsHall4, 8);
    path.AddEllipse(rectHall1);
    path.AddEllipse(rectHall2);
    Region reg1(&path);
    graphics->SetClip(&reg1, CombineMode::CombineModeXor);

    if (woodTexture1 != NULL) { // Закрашивает кабину
        TextureBrush shipCabinBrush(woodTexture1, WrapMode::WrapModeTile);
        graphics->FillRectangle(&shipCabinBrush, shipCabinRect);
    }
    else {
        SolidBrush shipCabinBrush(Color(214, 174, 1));
        graphics->FillRectangle(&shipCabinBrush, shipCabinRect);
    }


    graphics->DrawLine(&shipCabinLinePen, Point(x + 150, y), Point(x + 350, y));

    if (woodTexture2 != NULL) { // Рисует дверь
        TextureBrush shipDoorBrush(woodTexture2, WrapMode::WrapModeTile);
        graphics->FillClosedCurve(&shipDoorBrush, DoorPoints, DoorPointsCount,
                                 FillMode::FillModeAlternate, 0.3f);
    }
    else {
        SolidBrush shipDoorBrush(Color(204, 85, 0));
        graphics->FillClosedCurve(&shipDoorBrush, DoorPoints, DoorPointsCount,
            FillMode::FillModeAlternate, 0.3f);
    }

    graphics->DrawRectangle(&shipCabinPen, shipCabinRect);

    // *********** Иллюминатор коробля
    SolidBrush shipWindowBrush(Color(87, 176, 255));
    Pen        shipWindowPen(Color(25, 89, 209), 2.f);
    Rect       windowRect(x + 270, y - 35, 50, 25);

    graphics->FillRectangle(&shipWindowBrush, windowRect);
    graphics->DrawRectangle(&shipWindowPen, windowRect);

    // *********** Труба коробля
    Pen        shipPipePen(Color(42, 36, 28), 5.0f);
    HatchBrush shipPipeBrush(HatchStyle::HatchStyleLargeConfetti,
                           Color(94, 58, 4), Color(74, 54, 26));

    shipPipePen.SetEndCap(LineCap::LineCapRound);
    shipPipePen.SetStartCap(LineCap::LineCapRound);

    const int shipPipePointsCount = 4; // координаты трубы
    Point shipPipePoints[shipPipePointsCount] = {
        Point(x + 170, y - 50), Point(x + 200, y - 120),
        Point(x + 250, y - 120), Point(x + 220, y - 50)
    };

    graphics->DrawPolygon(&shipPipePen, shipPipePoints, shipPipePointsCount);
    graphics->FillPolygon(&shipPipeBrush, shipPipePoints, shipPipePointsCount);

    for (int i = 0; i < shipPipePointsCount; ++i) {
        shipPipePoints[i].X += 100;
    }

    graphics->DrawPolygon(&shipPipePen, shipPipePoints, shipPipePointsCount);
    graphics->FillPolygon(&shipPipeBrush, shipPipePoints, shipPipePointsCount);

    // *********** Крыша корабля
    Pen shipTopPen(Color(241, 160, 43), 15.0f);
    shipTopPen.SetStartCap(LineCap::LineCapRound);
    shipTopPen.SetEndCap(LineCap::LineCapRound);
    graphics->DrawLine(&shipTopPen, Point(x + 140, y - 50), Point(x + 360, y - 50));

    // *********** Корпус коробля.
    SolidBrush shipHullBrush(Color(92, 120, 240));
    Pen        shipHullPen(Color(84, 54, 14));

    const int shipHullPointsСount = 15;
    Point shipHullPoints[shipHullPointsСount] = {
        Point(x, y), Point(x + 110, y), Point(x + 120, y + 15),
        Point(x + 140, y + 30), Point(x + 350, y + 30), Point(x + 500, y + 5),
        Point(x + 510, y + 10), Point(x + 505, y + 50), Point(x + 450, y + 60),
        Point(x + 430, y + 70), Point(x + 400, y + 120), Point(x + 80, y + 120),
        Point(x + 50, y + 65), Point(x + 25, y + 50), Point(x + 10, y + 30)
    };

    graphics->FillClosedCurve(&shipHullBrush, shipHullPoints, shipHullPointsСount,
                             FillMode::FillModeAlternate, 0.25f);
    graphics->DrawClosedCurve(&shipHullPen, shipHullPoints, shipHullPointsСount, 0.25f);

    // *********** Линия на борту
    Pen   shipHullLinePen(Color(255, 0, 0), 10.0f);

    float partsPen[6] = {
        0.0f, 0.15f,
        0.45f, 0.65f,
        0.85f, 1.0f
    };
    
    shipHullLinePen.SetCompoundArray(partsPen, 6);
    shipHullLinePen.SetStartCap(LineCap::LineCapRound);
    shipHullLinePen.SetEndCap(LineCap::LineCapRound);

    const int shipHullLinePointCount = 3;
    Point shipHullLinePoint[shipHullLinePointCount] = {
        Point(x + 54, y + 65), Point(x + 250, y + 75), Point(x + 430, y + 65)
    };

    graphics->DrawCurve(&shipHullLinePen, shipHullLinePoint, shipHullLinePointCount);

    // *********** Якорь
    SolidBrush anchorBrush(Color(99, 80, 68));
    Pen        anchorPen(Color(87, 71, 61), 6.0f);

    Rect       anchorRect1(x + 40, y + 20, 10, 10);
    Rect       anchorRect2(x + 42, y + 25, 6, 40);
    Rect       anchorRect3(x + 35, y + 35, 20, 5);

    const int  anchorPointCount = 3;
    Point      anchorPoint[anchorPointCount] = {
        Point(x + 15, y + 35), Point(x + 45, y + 65), Point(x + 75, y + 35)
    };

    anchorPen.SetStartCap(LineCap::LineCapArrowAnchor);
    anchorPen.SetEndCap(LineCap::LineCapArrowAnchor);

    graphics->FillEllipse(&anchorBrush, anchorRect1);
    graphics->FillRectangle(&anchorBrush, anchorRect2);
    graphics->FillRectangle(&anchorBrush, anchorRect3);
    graphics->DrawCurve(&anchorPen, anchorPoint, anchorPointCount);

    reg1.MakeInfinite();
    graphics->SetClip(&reg1);
    delete[] pointsHall1;
    delete[] pointsHall2;
    delete[] pointsHall3;
    delete[] pointsHall4;
}

void DrawBackground(Graphics* graphics, RECT winRect) {
    Rect         skyRect(0, 0, winRect.right, winRect.bottom / 2);
    Rect         waterRect(0, winRect.bottom / 2, winRect.right, winRect.bottom / 2);
    TextureBrush skyBrush(skyTexture, WrapMode::WrapModeTile);
    TextureBrush waterBrush(waterTexture, WrapMode::WrapModeTile);

    graphics->FillRectangle(&skyBrush, skyRect);
    graphics->FillRectangle(&waterBrush, waterRect);
}

VOID OnPaint(HDC hdc, HWND hWnd)
{
    Graphics graphics(hdc);
    graphics.Clear(Color::White);
    graphics.SetSmoothingMode(SmoothingMode::SmoothingModeAntiAlias);
    graphics.SetTextRenderingHint(TextRenderingHint::TextRenderingHintAntiAliasGridFit);

    RECT winRect = { 0 };
    GetClientRect(hWnd, &winRect);
    Rect windowR(0, 0, winRect.right, winRect.bottom);

    GraphicsPath path;
    Rect ellipse(0, 0, winRect.right, winRect.bottom);
    path.AddEllipse(ellipse);
    Region reg1(&path);
    graphics.SetClip(&reg1);

    DrawBackground(&graphics, winRect);
    DrawShip(&graphics, winRect);

    reg1.Exclude(&path);
    reg1.Complement(windowR);
    graphics.SetClip(&reg1);

    if (shipImg != NULL) {
        float ratio = (float)shipImg->GetWidth() / (float)shipImg->GetHeight();
        REAL width = winRect.right / (REAL)3;

        graphics.DrawImage(shipImg, (REAL)0, (REAL)0, (REAL)width, (REAL)(width / ratio));
    }

    // Вывод текста
    SolidBrush fontBrush(Color::Black);
    Font font(L"Bookman Old Style", 30.0f, FontStyleBoldItalic);
    StringFormat strFormat;
    strFormat.SetAlignment(StringAlignment::StringAlignmentFar); // по горизонтали
    strFormat.SetLineAlignment(StringAlignment::StringAlignmentNear); // по вертикали
    graphics.DrawString(L"Пароход", -1, &font,
                        PointF((REAL)winRect.right, (REAL)0.0f),
                        &strFormat, &fontBrush);
}

// Генерирует массив из 3 точек.
PointF* GetShipBreakDown(PointF origin, int r) {
    PointF* points = new PointF[8];
    REAL r2 = (REAL)r * 3.f / 10.f;
    points[0] = PointF(origin.X - r, origin.Y);
    points[1] = PointF(origin.X - r2, origin.Y - r2);
    points[2] = PointF(origin.X, origin.Y - r);
    points[3] = PointF(origin.X + r2, origin.Y - r2);
    points[4] = PointF(origin.X + r, origin.Y);
    points[5] = PointF(origin.X + r2, origin.Y + r2);
    points[6] = PointF(origin.X, origin.Y + r);
    points[7] = PointF(origin.X - r2, origin.Y + r2);
    return points;
}